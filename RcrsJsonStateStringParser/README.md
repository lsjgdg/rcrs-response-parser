# rcrs json state string parser
change set의 파싱 기능을 담고 있는 클래스입니다. 이하는 이 객체에서 사용할 수 있는 메서드들입니다.

이 객체가 vector를 반환할 때 그 순서는 entity id의 오름차순으로 반환합니다.

## 파일 구조
 * `rcrsJsonStateStringParser.py`: chage set을 벡터화 시키는 클래스를 내장
 * `tester.py`: RcrsJsonStateStrngParser의 테스트 코드가 들어있습니다.

## import 하기
아래처럼 import할 수 있습니다.

```python
from rcrsJsonStateStringParser import RcrsJsonStateStringParser
```

## 사용 예제
```python
from rcrsJsonStateStringParser import RcrsJsonStateStringParser

def main(buldingEntityJsonString, RoadEntityJsonString, changeSetJsonString):
  parser = RcrsJsonStateStringParser()
  parser.addEntitys(buldingEntityJsonString, "building") # 건물의 초기값 등록
  parser.addEntitys(RoadEntityJsonString, "road") # 도로의 초기값 등록

  vectorList = parser.cumulativeParse(changeSetJsonString, -1) # 누적 파싱, 이번 change set에 없는 값은 이전 값에서 불러 오는 파싱법

  return vectorList
```

## 메서드
사용 가능한 메서드들입니다.

### constructor
`RcrsJsonStateStringParser()`

생성시 패러미터 필요 없음

### addEntitys
```addEntitys(initEntityJsonString, type)```

  1. `initEntityJsonString`: rcrs entity의 jsonString
  1. `type`: [string] 지금 등록하는 entity의 type

world에 이러한 entity가 있다고 parser에 등록하는 메서드입니다. parser는 여기서 등록된 entity에 대하서만 파싱합니다. 등록되지 않은 entity는 change set에 있어도 무시됩니다.

### getEntitys
```getEntitys()```

parser에 등록되어 있는 entity의 목록을 봅니다. key가 id(int)이고 value가 type(string)인 dictionary가 반환됩니다.

### parse
```parse(changeSetJsonString, fallback)```

 1. `changeSetJsonString`: rcrs의 change set의 json 포맷 string
 1. `fallback`: [any] 특정 property가 change set에 없을 때 기본값으로 쓸 값

change set을 파싱하여 vector의 배열로 반환합니다. 반환값은 2차원 배열입니다. 아래와 같이 반환됩니다.
```python
[
  [1,1,1,1,1,1,1,1], # 1번 entity의 vector화 결과
  [2,2,2,2,2,2,2,2], # 2번 entity의 vector화 결과
  [3,3,3,3,3,3,3,3], # 3번 entity의 vector화 결과
]
```
vector의 각 필드의 이름은 아래와 같습니다.

[x, y, damage, hp, stamina, waterquantity, fieryness, brokenness, blockades, temperature]

> 이 함수로 parsing한 결과는 cumulate되지 않습니다.

### linearParse
```linearParse(changeSetJsonString, fallback)```

 1. `changeSetJsonString`: rcrs의 change set의 json 포맷 string
 1. `fallback`: [any] 특정 property가 change set에 없을 때 기본값으로 쓸 값

change set을 파싱하여 vector(array)로 반환합니다. 반환값은 1차원 배열입니다. 아래와 같이 반환됩니다.

```python
[1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3]
```

```parse```와의 차이점은 1차원 벡터로 평탄화 한다는 것입니다.

> 이 함수로 parsing한 결과는 cumulate되지 않습니다.

### cumulativeParse
```cumulativeParse(changeSetJsonString)```

 1. `changeSetJsonString`: rcrs의 change set의 json 포맷 string

change set을 파싱하여 vector의 배열로 반환합니다. 반환값은 2차원 배열입니다. ```parse```와 차이점은 이번 change set에 없는 값은 이전의 값을 가져와 채운다는 것입니다.

> 이 메서드는 입력으로 들어온 change set을 기존 change set에 축적합니다.

### cumulativeLinearParse
```cumulativeLinearParse(changeSetJsonString)```

 1. `changeSetJsonString`: rcrs의 change set의 json 포맷 string

change set을 파싱하여 vector(array)로 반환합니다. 반환값은 1차원 배열입니다. ```cumulativeLinearParse```와 차이점은 1차원 배열로 평탄화하여 반환한다는 것입니다.

> 이 메서드는 입력으로 들어온 change set을 기존 change set에 축적합니다.

### goyalCumulativeParse
```cumulativeParse(changeSetJsonString)```

 1. `changeSetJsonString`: rcrs의 change set의 json 포맷 string

change set을 파싱하여 vector의 배열로 반환합니다. 반환값은 2차원 배열입니다.
goyal project에서 쓰인 형태로 벡터의 차원을 포맷팅합니다. 각 차원의 의미는 아래와 같습니다.

[temperature, fieryness, x, y, water quantity, health point]

> 이 메서드는 입력으로 들어온 change set을 기존 change set에 축적합니다.

### goyalCumulativeLinearParse
```goyalCumulativeLinearParse(changeSetJsonString)```

 1. `changeSetJsonString`: rcrs의 change set의 json 포맷 string

change set을 파싱하여 vector(array)로 반환합니다. 반환값은 1차원 배열입니다.
```goyalCumulativeParse```와 차이점은 1차원 배열로 평탄화하여 반환한다는 것입니다.

> 이 메서드는 입력으로 들어온 change set을 기존 change set에 축적합니다.

### cumulate
```cumulate(changeSetJsonString)```

 1. `changeSetJsonString`: rcrs의 change set의 json 포맷 string

 change set을 현재까지의 누적 change set에 누적시킵니다. 누적만하고 벡터로 포팅하지 않습니다. 결과값이 필요없고 누적만 해야할 때 사용하세요.

### getCumulativeChangeSetVectorList
```getCumulativeChangeSetVectorList()```

현재까지 누적된 change set을 벡터의 배열로 반환합니다. (2차원 벡터)

### getCumulativeChangeSetLinearVector
```getCumulativeChangeSetLinearVector()```

현재까지 누적된 change set을 1차원 벡터로 반환합니다.

### getCumulativeChangeSetVectorListByType
```getCumulativeChangeSetVectorListByType(type)```

1. `type`: [string] 결과로 받을 특정 type을 지정합니다. `addEntitys`로 등록할 때의 type과 일치해야 합니다.

현재까지 누적된 change set을 벡터의 리스트로 반환하되 특정 type의 entity만 반환합니다.

### getCumulativeChangeSetLinearVectorByType
```getCumulativeChangeSetLinearVectorByType(type)```

1. `type`: [string] 결과로 받을 특정 type을 지정합니다. `addEntitys`로 등록할 때의 type과 일치해야 합니다.

현재까지 누적된 change set을 1차원 벡터로 반환하되 특정 type의 entity만 반환합니다.

### getGoyalVectorList
```getGoyalVectorList()```

현재까지 누적된 chnage set을 goyal vector의 배열로 반환합니다.

### getGoyalLinearVector
```getGoyalLinearVector()```

현재까지 누적된 chnage set을 goyal vector 평탄화 벡터로 반환합니다.

### getGoyalVectorListByType
```getGoyalVectorListByType(type)```

 1. `type`: [string] 결과로 받을 특정 type을 지정합니다. `addEntitys`로 등록할 때의 type과 일치해야 합니다.

현재까지 누적된 change set을 goyal 벡터의 리스트로 반환하되 특정 type의 entity만 반환합니다.


### getGoyalLinearVectorByType
```getGoyalLinearVectorByType(type)```

 1. `type`: [string] 결과로 받을 특정 type을 지정합니다. `addEntitys`로 등록할 때의 type과 일치해야 합니다.

현재까지 누적된 change set을 goyal 평탄화 벡터로 반환하되 특정 type의 entity만 반환합니다.

### getFieldNameList
```getFieldNameList()```

벡터의 각 필드가 무엇을 나타내는 건지 배열로 반환합니다. 아래의 값을 반환합니다.
```python
["x", "y", "damage", "hp", "stamina", "waterquantity", "fieryness", "brokenness", "blockades", "temperature"]
```

## type
RcrsJsonStateStringParser에서 사용되는 string들의 포맷입니다.

### initEntityJsonString
rcrs에서 `getAllEntity()`를 통해 얻을 수 있는 값입니다. 현재 Entity의 상태를 나타냅니다. 아래와 같은 형태를 가지고 있습니다.

```json
[{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":198},"totalArea":{"value":198},"temperature":{"value":0},"importance":{"value":1},"x":{"value":19947},"y":{"value":125037},"id":{"id":298}},{"floors":{"value":3},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":138},"totalArea":{"value":414},"temperature":{"value":0},"importance":{"value":1},"x":{"value":42666},"y":{"value":91869},"id":{"id":905}},{"floors":{"value":2},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":2},"attributes":{"value":0},"groundArea":{"value":204},"totalArea":{"value":408},"temperature":{"value":0},"importance":{"value":1},"x":{"value":43853},"y":{"value":31341},"id":{"id":934}},{"floors":{"value":3},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":252},"totalArea":{"value":756},"temperature":{"value":0},"importance":{"value":1},"x":{"value":38500},"y":{"value":52000},"id":{"id":935}},{"floors":{"value":3},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":281},"totalArea":{"value":843},"temperature":{"value":0},"importance":{"value":1},"x":{"value":42421},"y":{"value":77033},"id":{"id":936}},{"floors":{"value":3},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":1},"attributes":{"value":0},"groundArea":{"value":241},"totalArea":{"value":723},"temperature":{"value":0},"importance":{"value":1},"x":{"value":64347},"y":{"value":78347},"id":{"id":937}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":1},"attributes":{"value":0},"groundArea":{"value":132},"totalArea":{"value":132},"temperature":{"value":0},"importance":{"value":1},"x":{"value":40000},"y":{"value":110500},"id":{"id":938}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":66},"totalArea":{"value":66},"temperature":{"value":0},"importance":{"value":1},"x":{"value":53500},"y":{"value":113000},"id":{"id":939}},{"floors":{"value":3},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":55},"totalArea":{"value":165},"temperature":{"value":0},"importance":{"value":1},"x":{"value":53500},"y":{"value":107500},"id":{"id":940}},{"floors":{"value":3},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":136},"totalArea":{"value":408},"temperature":{"value":0},"importance":{"value":1},"x":{"value":67000},"y":{"value":107500},"id":{"id":941}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":140},"totalArea":{"value":140},"temperature":{"value":0},"importance":{"value":1},"x":{"value":81000},"y":{"value":110000},"id":{"id":942}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":157},"totalArea":{"value":157},"temperature":{"value":0},"importance":{"value":1},"x":{"value":98709},"y":{"value":111840},"id":{"id":943}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":1},"attributes":{"value":0},"groundArea":{"value":119},"totalArea":{"value":119},"temperature":{"value":0},"importance":{"value":1},"x":{"value":87176},"y":{"value":89411},"id":{"id":944}},{"floors":{"value":2},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":57},"totalArea":{"value":114},"temperature":{"value":0},"importance":{"value":1},"x":{"value":119275},"y":{"value":100101},"id":{"id":945}},{"floors":{"value":2},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":50},"totalArea":{"value":100},"temperature":{"value":0},"importance":{"value":1},"x":{"value":120000},"y":{"value":93500},"id":{"id":946}},{"floors":{"value":2},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":50},"totalArea":{"value":100},"temperature":{"value":0},"importance":{"value":1},"x":{"value":120000},"y":{"value":87500},"id":{"id":947}},{"floors":{"value":3},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":1},"attributes":{"value":0},"groundArea":{"value":50},"totalArea":{"value":150},"temperature":{"value":0},"importance":{"value":1},"x":{"value":120000},"y":{"value":81500},"id":{"id":948}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":50},"totalArea":{"value":50},"temperature":{"value":0},"importance":{"value":1},"x":{"value":120000},"y":{"value":75500},"id":{"id":949}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":50},"totalArea":{"value":50},"temperature":{"value":0},"importance":{"value":1},"x":{"value":120000},"y":{"value":69500},"id":{"id":950}},{"floors":{"value":2},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":50},"totalArea":{"value":100},"temperature":{"value":0},"importance":{"value":1},"x":{"value":120000},"y":{"value":63500},"id":{"id":951}},{"floors":{"value":2},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":50},"totalArea":{"value":100},"temperature":{"value":0},"importance":{"value":1},"x":{"value":120000},"y":{"value":57500},"id":{"id":952}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":394},"totalArea":{"value":394},"temperature":{"value":0},"importance":{"value":1},"x":{"value":95081},"y":{"value":66758},"id":{"id":953}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":1},"attributes":{"value":0},"groundArea":{"value":50},"totalArea":{"value":50},"temperature":{"value":0},"importance":{"value":1},"x":{"value":120000},"y":{"value":51500},"id":{"id":954}},{"floors":{"value":2},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":50},"totalArea":{"value":100},"temperature":{"value":0},"importance":{"value":1},"x":{"value":120000},"y":{"value":45500},"id":{"id":955}},{"floors":{"value":3},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":50},"totalArea":{"value":150},"temperature":{"value":0},"importance":{"value":1},"x":{"value":120000},"y":{"value":39500},"id":{"id":956}},{"floors":{"value":3},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":50},"totalArea":{"value":150},"temperature":{"value":0},"importance":{"value":1},"x":{"value":120000},"y":{"value":33500},"id":{"id":957}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":50},"totalArea":{"value":50},"temperature":{"value":0},"importance":{"value":1},"x":{"value":120000},"y":{"value":27500},"id":{"id":958}},{"floors":{"value":2},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":324},"totalArea":{"value":648},"temperature":{"value":0},"importance":{"value":1},"x":{"value":95000},"y":{"value":36000},"id":{"id":959}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":244},"totalArea":{"value":244},"temperature":{"value":0},"importance":{"value":1},"x":{"value":70000},"y":{"value":35369},"id":{"id":960}},{"floors":{"value":2},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":100},"totalArea":{"value":200},"temperature":{"value":0},"importance":{"value":1},"x":{"value":10000},"y":{"value":50000},"id":{"id":247}},{"floors":{"value":2},"ignition":{"value":true},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":1},"attributes":{"value":0},"groundArea":{"value":100},"totalArea":{"value":200},"temperature":{"value":0},"importance":{"value":1},"x":{"value":45000},"y":{"value":5000},"id":{"id":248}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":500},"totalArea":{"value":500},"temperature":{"value":0},"importance":{"value":1},"x":{"value":9000},"y":{"value":75000},"id":{"id":249}},{"floors":{"value":2},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":125},"totalArea":{"value":250},"temperature":{"value":0},"importance":{"value":1},"x":{"value":125500},"y":{"value":118500},"id":{"id":250}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":1},"attributes":{"value":0},"groundArea":{"value":190},"totalArea":{"value":190},"temperature":{"value":0},"importance":{"value":1},"x":{"value":65000},"y":{"value":9500},"id":{"id":251}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":200},"totalArea":{"value":200},"temperature":{"value":0},"importance":{"value":1},"x":{"value":69993},"y":{"value":130991},"id":{"id":253}},{"floors":{"value":1},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":0},"attributes":{"value":0},"groundArea":{"value":440},"totalArea":{"value":440},"temperature":{"value":0},"importance":{"value":1},"x":{"value":150727},"y":{"value":82727},"id":{"id":254}},{"floors":{"value":2},"ignition":{"value":false},"fieryness":{"value":0},"brokenness":{"value":0},"code":{"value":1},"attributes":{"value":0},"groundArea":{"value":155},"totalArea":{"value":310},"temperature":{"value":0},"importance":{"value":1},"x":{"value":131646},"y":{"value":12024},"id":{"id":255}}]
```

entity의 종류마다 모양은 다릅니다. 위는 building의 initEntityJsonString을 보여주고 있습니다.

### changeSetJsonString

rcrs가 매 step마다 생성하는 데이터입니다. 이전 상태 대비 무엇이 바뀌었는지를 표현하고 있습니다. 아래와 같은 형태입니다.

```json
{"deleted": [], "changes": {"2100749373": {"urn:rescuecore2.standard:property:y": {"value": 35632}, "urn:rescuecore2.standard:property:apexes": {"data": [131760, 24966, 130716, 26951, 131431, 27811, 128356, 30369, 126840, 28546, 126840, 29000, 126840, 34000, 126840, 36000, 126840, 37000, 126840, 39000, 126840, 44915, 128827, 44260, 130582, 43189, 132046, 41761, 133159, 40036, 133160, 40036, 133160, 26963, 132194, 25424, 131760, 24966]}, "urn:rescuecore2.standard:property:x": {"value": 130014}, "urn:rescuecore2.standard:property:repaircost": {"value": 95}}, "1557027963": {"urn:rescuecore2.standard:property:position": {"value": {"id": 254}}, "urn:rescuecore2.standard:property:y": {"value": 89800}, "urn:rescuecore2.standard:property:x": {"value": 131199}, "urn:rescuecore2.standard:property:positionhistory": {"data": [131199, 89800, 131199, 89800, 131199, 89800, 131199, 89800, 131199, 89800, 131199, 89800, 131199, 89800, 131199, 89800, 131199, 89800, 131199, 89800]}, "urn:rescuecore2.standard:property:damage": {"value": 0}, "urn:rescuecore2.standard:property:traveldistance": {"value": 0}}, "2100749341": {"urn:rescuecore2.standard:property:y": {"value": 28285}, "urn:rescuecore2.standard:property:apexes": {"data": [125499, 27000, 125499, 29000, 126840, 29000, 126840, 28546, 125553, 27000]}, "urn:rescuecore2.standard:property:x": {"value": 126027}, "urn:rescuecore2.standard:property:repaircost": {"value": 1}}, "410064826": {"urn:rescuecore2.standard:property:position": {"value": {"id": 254}}, "urn:rescuecore2.standard:property:y": {"value": 87707}, "urn:rescuecore2.standard:property:x": {"value": 133411}, "urn:rescuecore2.standard:property:positionhistory": {"data": [133411, 87708, 133411, 87706, 133410, 87703, 133409, 87705, 133409, 87707, 133409, 87708, 133410, 87707, 133411, 87707, 133411, 87708, 133411, 87707]}, "urn:rescuecore2.standard:property:damage": {"value": 0}, "urn:rescuecore2.standard:property:traveldistance": {"value": 10}}, "1102153632": {"urn:rescuecore2.standard:property:position": {"value": {"id": 254}}, "urn:rescuecore2.standard:property:y": {"value": 89800}, "urn:rescuecore2.standard:property:x": {"value": 133983}, "urn:rescuecore2.standard:property:positionhistory": {"data": [133983, 89800, 133988, 89800, 133995, 89800, 133991, 89800, 133985, 89800, 133984, 89800, 133984, 89800, 133983, 89800, 133983, 89800, 133983, 89800]}, "urn:rescuecore2.standard:property:damage": {"value": 0}, "urn:rescuecore2.standard:property:traveldistance": {"value": 23}}, "248": {"urn:rescuecore2.standard:property:temperature": {"value": 123}}, "2100749274": {"urn:rescuecore2.standard:property:position": {"value": {"id": 254}}, "urn:rescuecore2.standard:property:y": {"value": 87718}, "urn:rescuecore2.standard:property:x": {"value": 131199}, "urn:rescuecore2.standard:property:positionhistory": {"data": [131199, 87718, 131199, 87720, 131199, 87723, 131199, 87723, 131199, 87721, 131199, 87719, 131199, 87719, 131199, 87718, 131199, 87718, 131199, 87718]}, "urn:rescuecore2.standard:property:damage": {"value": 0}, "urn:rescuecore2.standard:property:traveldistance": {"value": 10}}, "2100749342": {"urn:rescuecore2.standard:property:y": {"value": 28163}, "urn:rescuecore2.standard:property:apexes": {"data": [125499, 27000, 125499, 29000, 126207, 29000, 126207, 27785, 125553, 27000]}, "urn:rescuecore2.standard:property:x": {"value": 125822}, "urn:rescuecore2.standard:property:repaircost": {"value": 1}}, "1962675462": {"urn:rescuecore2.standard:property:positionhistory": {"data": []}, "urn:rescuecore2.standard:property:damage": {"value": 0}, "urn:rescuecore2.standard:property:traveldistance": {"value": 0}}, "2100749429": {"urn:rescuecore2.standard:property:y": {"value": 28133}, "urn:rescuecore2.standard:property:apexes": {"data": [125499, 27000, 125499, 29000, 126086, 29000, 126086, 27640, 125553, 27000]}, "urn:rescuecore2.standard:property:x": {"value": 125772}, "urn:rescuecore2.standard:property:repaircost": {"value": 1}}, "1548259968": {"urn:rescuecore2.standard:property:position": {"value": {"id": 254}}, "urn:rescuecore2.standard:property:y": {"value": 89800}, "urn:rescuecore2.standard:property:x": {"value": 132076}, "urn:rescuecore2.standard:property:positionhistory": {"data": [132076, 89800, 132101, 89800, 132078, 89800, 132074, 89800, 132079, 89800, 132078, 89800, 132076, 89800, 132076, 89800, 132076, 89800, 132076, 89800]}, "urn:rescuecore2.standard:property:damage": {"value": 0}, "urn:rescuecore2.standard:property:traveldistance": {"value": 69}}, "482151809": {"urn:rescuecore2.standard:property:positionhistory": {"data": []}, "urn:rescuecore2.standard:property:damage": {"value": 0}, "urn:rescuecore2.standard:property:traveldistance": {"value": 0}}, "210552869": {"urn:rescuecore2.standard:property:positionhistory": {"data": []}, "urn:rescuecore2.standard:property:damage": {"value": 0}, "urn:rescuecore2.standard:property:traveldistance": {"value": 0}}}, "entityURNs": {"2100749373": "urn:rescuecore2.standard:entity:blockade", "1557027963": "urn:rescuecore2.standard:entity:civilian", "2100749341": "urn:rescuecore2.standard:entity:blockade", "410064826": "urn:rescuecore2.standard:entity:civilian", "1102153632": "urn:rescuecore2.standard:entity:civilian", "248": "urn:rescuecore2.standard:entity:building", "2100749274": "urn:rescuecore2.standard:entity:civilian", "2100749342": "urn:rescuecore2.standard:entity:blockade", "1962675462": "urn:rescuecore2.standard:entity:policeforce", "2100749429": "urn:rescuecore2.standard:entity:blockade", "1548259968": "urn:rescuecore2.standard:entity:civilian", "482151809": "urn:rescuecore2.standard:entity:ambulanceteam", "210552869": "urn:rescuecore2.standard:entity:firebrigade"}}
```
# 의존성
# mongoDB
# pip install pymongo
import json

class RcrsJsonStateStringParser:
    def __init__(self):
        self.entityDict = {} #id-type piar
        self.cumulateChangeSet = {}

    def addEntitys(self, jsonString, type):
        jsonArray = json.loads(jsonString)

        for ob in jsonArray:
            entityId = ob["id"]["id"]
            self.entityDict[entityId] = type
            self.cumulateChangeSet[entityId] = initInfoParser(ob, -1);

    def getEntitys(self):
        return self.entityDict

    def parse(self, jsonString, fallback):
        jsonObject = json.loads(jsonString)
        vectorList = []

        for id, type in sorted(self.entityDict.items()):
            object = None
            try:
                object = jsonObject["changes"][str(id)]
            except:
                object = None

            vector = infoParser(object, fallback);

            vectorList.append(vector)

        return vectorList

    def linearParse(self, jsonString, fallback):
        vectorList = self.parse(jsonString, fallback)

        linearArray = []

        for vector in vectorList:
            for value in vector:
                linearArray.append(value)

        return linearArray

    def getFieldNameList(self):
        return ["x", "y", "damage", "hp", "stamina", "waterquantity", "fieryness", "brokenness", "blockades", "temperature"]

    def cumulativeParse(self, jsonString):
        self.cumulate(jsonString)
        return self.getCumulativeChangeSetVectorList()

    def cumulate(self, jsonString):
        jsonObject = json.loads(jsonString)

        for id, type in self.entityDict.items():
            object = None
            try:
                object = jsonObject["changes"][str(id)]
            except:
                #이번 step에서 변경되지 않음
                continue

            vector = infoParser(object, None)

            # cumulate
            for i in range(0, len(vector)):
                value = vector[i]
                if(value != None):
                    self.cumulateChangeSet[id][i] = value

    def cumulativeLinearParse(self, jsonString):
        self.cumulate(jsonString)
        return self.getCumulativeChangeSetLinearVector()

    def getCumulativeChangeSetVectorList(self):
        vectorList = []
        for id, vector in sorted(self.cumulateChangeSet.items()):
            vectorList.append(vector)

        return vectorList;

    def getCumulativeChangeSetLinearVector(self):
        vectorList = self.getCumulativeChangeSetVectorList()

        linearArray = []

        for vector in vectorList:
            for value in vector:
                linearArray.append(value)

        return linearArray

    def getCumulativeChangeSetVectorListByType(self, type):
        vectorList = []
        for id, vector in sorted(self.cumulateChangeSet.items()):
            if self.entityDict[id] == type:
                vectorList.append(vector)

        return vectorList

    def getCumulativeChangeSetLinearVectorByType(self, type):
        vectorList = self.getCumulativeChangeSetVectorListByType(type)

        linearArray = []

        for vector in vectorList:
            for value in vector:
                linearArray.append(value)

        return linearArray

    def goyalCumulativeParse(self, jsonString):
        self.cumulate(jsonString)
        return self.getGoyalVectorList()

    def goyalCumulativeLinearParse(self, jsonString):
        self.cumulate(jsonString)
        return self.getGoyalLinearVector()

    def getGoyalVectorList(self):
        vectorList = []
        for id, vector in sorted(self.cumulateChangeSet.items()):
            # 9 : temperature
            # 6 : fieryness
            # 0 : x
            # 1 : y
            # 5 : waterquantity
            # 3 : health point
            # 위 정보를 vector에서 뽑아내자.
            goyalVector = []
            goyalVector.append(vector[9])
            goyalVector.append(vector[6])
            goyalVector.append(vector[0])
            goyalVector.append(vector[1])
            goyalVector.append(vector[5])
            goyalVector.append(vector[3])
            vectorList.append(goyalVector)

        return vectorList

    def getGoyalLinearVector(self):
        goyalVectorList = self.getGoyalVectorList()

        linearArray = []

        for vector in goyalVectorList:
            for value in vector:
                linearArray.append(value)

        return linearArray

    def getGoyalVectorListByType(self, type):
        vectorList = []
        for id, vector in sorted(self.cumulateChangeSet.items()):
            if self.entityDict[id] == type:
                goyalVector = []
                goyalVector.append(vector[9])
                goyalVector.append(vector[6])
                goyalVector.append(vector[0])
                goyalVector.append(vector[1])
                goyalVector.append(vector[5])
                goyalVector.append(vector[3])
                vectorList.append(goyalVector)

        return vectorList

    def getGoyalLinearVectorByType(self, type):
        goyalVectorList = self.getGoyalVectorListByType(type)

        linearArray = []

        for vector in goyalVectorList:
            for value in vector:
                linearArray.append(value)

        return linearArray





# ----------------------------

#범용 함수들

def infoParser(object, fallback):
    vector = []


    vector.append(tryGetValue(object, "x", "value", fallback))
    vector.append(tryGetValue(object, "y", "value", fallback))
    vector.append(tryGetValue(object, "damage", "value", fallback))
    vector.append(tryGetValue(object, "hp", "value", fallback))
    vector.append(tryGetValue(object, "stamina", "value", fallback))
    vector.append(tryGetValue(object, "waterquantity", "value", fallback))
    vector.append(tryGetValue(object, "fieryness", "value", fallback))
    vector.append(tryGetValue(object, "brokenness", "value", fallback))
    vector.append(tryGetValue(object, "temperature", "value", fallback))

    blockadeList = tryGetValue(object, "blockades", "ids", fallback)
    if(isinstance(blockadeList, list) == False):
        vector.append(fallback)
    else:
        vector.append(len(blockadeList)) #길에 있는 blockade의 개수

    return vector

# 첫 entity 정보 파싱용 함수
def initInfoParser(initObject, fallback):
    vector = []

    vector.append(tryGetInitValue(initObject, "x", "value", fallback))
    vector.append(tryGetInitValue(initObject, "y", "value", fallback))
    vector.append(tryGetInitValue(initObject, "damage", "value", fallback))
    vector.append(tryGetInitValue(initObject, "hp", "value", fallback))
    vector.append(tryGetInitValue(initObject, "stamina", "value", fallback))
    vector.append(tryGetInitValue(initObject, "water", "value", fallback))
    vector.append(tryGetInitValue(initObject, "fieryness", "value", fallback))
    vector.append(tryGetInitValue(initObject, "brokenness", "value", fallback))
    vector.append(tryGetInitValue(initObject, "temperature", "value", fallback))

    blockadeList = tryGetInitValue(object, "blockades", "ids", fallback)
    if(isinstance(blockadeList, list) == False):
        vector.append(fallback)
    else:
        vector.append(len(blockadeList)) #길에 있는 blockade의 개수

    return vector

def tryGetValue(object, propertyName, propertyAttribute, fallback):
    value = None
    try:
        value = object["urn:rescuecore2.standard:property:" + propertyName][propertyAttribute]
    except:
        value = fallback

    return value

def tryGetInitValue(object, propertyName, propertyAttribute, fallback):
    value = None
    try:
        value = object[propertyName][propertyAttribute]
    except:
        value = fallback

    return value

def getAveragePoint(pointList):
    # 벡터 평균 내기
    xSum = 0
    ySum = 0
    count = len(pointList)/2
    for i in range(0, len(pointList), 2):
        xSum = xSum + pointList[i]
        ySum = ySum + pointList[i+1]

    return {"x": xSum/count, "y": ySum/count}

# rcrs json state string parser
RCRS용 유틸이 들어 있는 레포지터리입니다.

## 폴더 구조
  * `imagifier`: 벡터를 이미지로 바꾸는 유틸이 들어있습니다. 해당 폴더의 `README.md`를 참조하세요.
  * `RcrsJsonStateStringParser`: RCRS가 매 시간 간격마다 내놓는 change set 응답의 포맷을 바꿀 수 있는 유틸이 들어있습니다. 해당 폴더의 `README.md`를 참조하세요.

## 필요한 의존성
아래의 모듈이 깔려 있어야 합니다.
```shell
pip3 install scikit-learn
pip3 install json
pip3 install numpy
```